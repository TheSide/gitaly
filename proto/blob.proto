syntax = "proto3";

package gitaly;

option go_package = "gitlab.com/gitlab-org/gitaly/proto/go/gitalypb";

import "lint.proto";
import "shared.proto";

service BlobService {
  // GetBlob returns the contents of a blob object referenced by its object
  // ID. We use a stream to return a chunked arbitrarily large binary
  // response
  rpc GetBlob(GetBlobRequest) returns (stream GetBlobResponse) {
    option (op_type) = {
      op: ACCESSOR
    };
  }
  rpc GetBlobs(GetBlobsRequest) returns (stream GetBlobsResponse) {
    option (op_type) = {
      op: ACCESSOR
    };
  }

  // GetLFSPointers retrieves LFS pointers from a given set of object IDs.
  // This RPC filters all requested objects and only returns those which refer
  // to a valid LFS pointer.
  rpc GetLFSPointers(GetLFSPointersRequest) returns (stream GetLFSPointersResponse) {
    option (op_type) = {
      op: ACCESSOR
    };
  }

  // GetNewLFSPointers retrieves LFS pointers for a limited subset of the
  // commit graph. It will return all LFS pointers which are reachable by the
  // provided revision, but not reachable by any of the limiting references.
  rpc GetNewLFSPointers(GetNewLFSPointersRequest) returns (stream GetNewLFSPointersResponse) {
    option (op_type) = {
      op: ACCESSOR
    };
  }

  // GetAllLFSPointers retrieves all LFS pointers of the given repository.
  rpc GetAllLFSPointers(GetAllLFSPointersRequest) returns (stream GetAllLFSPointersResponse) {
    option (op_type) = {
      op: ACCESSOR
    };
  }
}

message GetBlobRequest {

  Repository repository = 1[(target_repository)=true];
  // Object ID (SHA1) of the blob we want to get
  string oid = 2;
  // Maximum number of bytes we want to receive. Use '-1' to get the full blob no matter how big.
  int64 limit = 3;
}

message GetBlobResponse {
  // Blob size; present only in first response message
  int64 size = 1;
  // Chunk of blob data
  bytes data = 2;
  // Object ID of the actual blob returned. Empty if no blob was found.
  string oid = 3;
}

message GetBlobsRequest {

  message RevisionPath {
    string revision = 1;
    bytes path = 2;
  }

  Repository repository = 1[(target_repository)=true];
  // Revision/Path pairs of the blobs we want to get.
  repeated RevisionPath revision_paths = 2;
  // Maximum number of bytes we want to receive. Use '-1' to get the full blobs no matter how big.
  int64 limit = 3;
}

message GetBlobsResponse {
  // Blob size; present only on the first message per blob
  int64 size = 1;
  // Chunk of blob data, could span over multiple messages.
  bytes data = 2;
  // Object ID of the current blob. Only present on the first message per blob. Empty if no blob was found.
  string oid = 3;
  bool is_submodule = 4;
  int32 mode = 5;
  string revision = 6;
  bytes path = 7;
  ObjectType type = 8;
}

// LFSPointer is a git blob which points to an LFS object.
message LFSPointer {
  // Size is the size of the blob. This is not the size of the LFS object
  // pointed to.
  int64 size = 1;
  // Data is the bare data of the LFS pointer blob. It contains the pointer to
  // the LFS data in the format specified by the LFS project.
  bytes data = 2;
  // Oid is the object ID of the blob.
  string oid = 3;
}

message NewBlobObject {
  int64 size = 1;
  string oid = 2;
  bytes path = 3;
}

// GetLFSPointersRequest is a request for the GetLFSPointers RPC.
message GetLFSPointersRequest {
  // Repository is the repository for which LFS pointers should be retrieved
  // from.
  Repository repository = 1[(target_repository)=true];
  // BlobIds is the list of blobs to retrieve LFS pointers from. Must be a
  // non-empty list of blobs IDs to fetch.
  repeated string blob_ids = 2;
}

// GetLFSPointersResponse is a response for the GetLFSPointers RPC.
message GetLFSPointersResponse {
  // LfsPointers is the list of LFS pointers which were requested.
  repeated LFSPointer lfs_pointers = 1;
}

// GetNewLFSPointersRequest is a request for the GetNewLFSPointers RPC.
message GetNewLFSPointersRequest {
  // Repository is the repository for which LFS pointers should be retrieved
  // from.
  Repository repository = 1[(target_repository)=true];
  // Revision is the revision for which to retrieve new LFS pointers.
  bytes revision = 2;
  // Limit limits the number of LFS pointers returned.
  int32 limit = 3;
  // NotInAll limits the revision graph to not include any commits which are
  // referenced by a git reference. When `not_in_all` is true, `not_in_refs` is
  // ignored.
  bool not_in_all = 4;
  // NotInRefs is a list of references used to limit the revision graph. Any
  // commit reachable by any commit in NotInRefs will not be searched for new
  // LFS pointers. This is ignored if NotInAll is set to `true`.
  repeated bytes not_in_refs = 5;
}

// GetNewLFSPointersResponse is a response for the GetNewLFSPointers RPC.
message GetNewLFSPointersResponse {
  // LfsPointers is the list of LFS pointers which were requested.
  repeated LFSPointer lfs_pointers = 1;
}

// GetAllLFSPointersRequest is a request for the GetAllLFSPointers RPC.
message GetAllLFSPointersRequest {
  // Repository is the repository for which LFS pointers shoul be retrieved
  // from.
  Repository repository = 1[(target_repository)=true];
  reserved 2;
}

// GetAllLFSPointersResponse is a response for the GetAllLFSPointers RPC.
message GetAllLFSPointersResponse {
  // LfsPointers is the list of LFS pointers.
  repeated LFSPointer lfs_pointers = 1;
}
